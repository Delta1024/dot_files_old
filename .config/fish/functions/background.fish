# Defined in - @ line 1
function background --wraps='/usr/bin/feh --bg-scale' --description 'alias background /usr/bin/feh --bg-scale'
  /usr/bin/feh --bg-scale $argv;
end
