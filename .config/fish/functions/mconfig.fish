# Defined in - @ line 1
function mconfig --wraps='vim ~/.xomnad/xmonad.hs' --description 'alias mconfig vim ~/.xomnad/xmonad.hs'
  vim ~/.xmonad/xmonad.hs $argv;
end
