# Defined in - @ line 1
function config --wraps='/bin/git --git-dir=$HOME/.dot_files/ --work-tree=$HOME' --description 'alias config /bin/git --git-dir=$HOME/.dot_files/ --work-tree=$HOME'
  /bin/git --git-dir=$HOME/.dot_files/ --work-tree=$HOME $argv;
end
